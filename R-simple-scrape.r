#Create a list of required packages 
packages <- c("dplyr", "tidyverse", "ggplot2", "rvest")


#Funtion to install and call all necessary packages
EnsurePackage<-function(x)
{
  x<-as.character(x)
  if (!require(x,character.only=TRUE))
  {
    install.packages(pkgs=x,dependencies = TRUE)
    require(x,character.only=TRUE)
  }
}

#Run a for loop to call all packages
for (package in packages) {
  EnsurePackage(package)
}

# Start by reading a HTML page with read_html():
html_page <- read_html("https://www.scrapethissite.com/pages/simple/")

# Extract the country cards from the webpage
country_cards <- html_page %>%
  html_nodes("div.col-md-4.country")

# Extract the country names from the HTML nodes:
country_names <- country_cards %>%
  html_elements("h3.country-name") %>%
  html_text2()

# Extract the capitals of the countries
country_capitals <- country_cards %>%
  html_elements("span.country-capital") %>%
  html_text2()

# Extract the population of the countries
country_populations <- country_cards %>%
  html_elements("span.country-population") %>%
  html_text2()

# Extract the population of the area
country_area <- country_cards %>%
  html_elements("span.country-area") %>%
  html_text2()

# Create a dataframe of the 
country_data <- data.frame(
  Country = country_names,
  Capital = country_capitals,
  Population = country_populations,
  Area = country_area
)
